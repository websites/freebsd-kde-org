---
title: 'KDE Gear 21.04.1 landed'
date: 2021-05-13 20:15:00
layout: post
---

The latest monthly version of applications, libraries and
other material -- all the KDE Gear, previously knows as *Release Service*
or *Applications*, but much broader than that --
is available
in the official FreeBSD ports tree.
