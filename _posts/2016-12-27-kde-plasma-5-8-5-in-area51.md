---
title: 'KDE Plasma 5.8.5 in area51'
date: 2016-12-27 00:00:00 
layout: post
---

<p>
The KDE-FreeBSD team has updated the unofficial ports tree, area51,
with the latest release of <a href="https://www.kde.org/announcements/plasma-5.8.5.php">KDE Plasma, 5.8.5</a>.
This version is available from the unofficial ports tree for KDE-FreeBSD,
<a href="https://freebsd.kde.org/area51.php">area51</a>, in the plasma5/ branch.
    </p>