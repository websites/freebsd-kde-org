---
title: 'KDE Release Service 20.12.3 landed'
date: 2021-03-04 20:15:00
layout: post
---

The latest monthly version of applications, libraries and
other material from the KDE Release Service -- previously KDE Applications --
is available
in the official FreeBSD ports tree.
