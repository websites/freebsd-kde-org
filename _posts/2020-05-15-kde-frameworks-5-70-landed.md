---
title: 'KDE Frameworks 5.70 landed'
date: 2020-05-15 18:25:00
layout: post
---

The latest monthly version of KDE Frameworks is available
in the official FreeBSD ports tree.
