---
title: 'KDE Frameworks 5.68 landed'
date: 2020-03-21 07:30:00
layout: post
---

The latest monthly version of KDE Frameworks is available
in the official FreeBSD ports tree.
