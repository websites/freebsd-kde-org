---
title: 'KDE Frameworks 5.71 landed'
date: 2020-06-17 10:45:00
layout: post
---

The latest monthly version of KDE Frameworks is available
in the official FreeBSD ports tree.
