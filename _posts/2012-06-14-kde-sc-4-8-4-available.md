---
title: 'KDE SC 4.8.4 available'
date: 2012-06-14 00:00:00 
layout: post
---

<p>The <b>last update in the 4.8 series</b> was just committed to ports. You might want to read the <a href="http://kde.org/announcements/announce-4.8.4.php">release announcement</a>.</p><p>Meanwhile, we were able to fix two serious bugs in <i>Akonadi</i> which were preventing KMail 2 and other Akonadi-enabled software from working properly. <b>KDE-Pim 4.8.4</b> should now be <i>safe to use</i>.</p><p>Happy updating!</p>