---
title: 'KDE SC 4.7.4 released'
date: 2012-01-25 00:00:00 
layout: post
---

<p>The KDE/FreeBSD team is pleased to announce KDE Software Compilation 4.7.4, which concludes the 4.7 series. We invite you to read the <a href="http://kde.org/announcements/announce-4.7.4.php">original announcement</a>.</p><p>Have a nice upgrade!</p>