---
title: 'Qt 5.7.1, KDE Frameworks 5.31 landed'
date: 2017-02-18 00:00:00 
layout: post
---

<p>
The KDE-FreeBSD team is happy to announce the immediate availability
of <a href="http://blog.qt.io/blog/2016/12/14/qt-5-7-1-released/">Qt 5.7.1</a> and <a href="https://www.kde.org/announcements/kde-frameworks-5.31.0.php">KDE Frameworks 5.31</a> in the official FreeBSD ports tree; check out <a hfref="http://www.freshports.org/">FreshPorts</a> for the latest ports news.
    </p><p>
This release adds one new KDE Framework, Kirigami 2.
It also massages the Qt4 and Qt5 ports to live together more
harmoniously. In particular, it adds <i>misc/qtchooser</i>, which
allows developers and sysadmins to manage multiple concurrent Qt
installations. We advise users to consult the UPDATING entry for
20170218.
    </p>