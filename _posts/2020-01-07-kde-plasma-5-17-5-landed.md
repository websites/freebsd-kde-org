---
title: 'KDE Plasma 5.17.5 landed'
date: 2020-01-07 18:00:00
layout: post
---

The latest version of KDE Plasma Desktop is available in the official FreeBSD ports tree.
