---
title: 'KDE Plasma 5.19.1 landed'
date: 2020-06-16 17:20:00
layout: post
---

The latest version of KDE Plasma Desktop is available in the official FreeBSD ports tree.
