---
title: 'New Committer'
date: 2016-06-06 00:00:00 
layout: post
---

<p>
Tobias Berner, long-time KDE-FreeBSD contributor, has been accepted
as a FreeBSD ports-committer. This means that the team of people
actively working on not just area51 ports, but also the official ports,
has expanded by one. This is expected to improve the speed at
which unofficial ports get turned into official ports.
    </p>