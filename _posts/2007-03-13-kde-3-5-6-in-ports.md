---
title: 'KDE 3.5.6 in ports'
date: 2007-03-13 00:00:00 
layout: post
---

<p>KDE 3.5.6 has been committed to ports.</p><p>UPDATING:</p><pre>
20070313:
  AFFECTS: users of x11/kdebase3
  AUTHOR: kde@FreeBSD.org

  The HAL option has been turned on by default and the fstabbackend
  patches which have kept the fstab backend for the media kioslave
  at the status quo of KDE 3.5.3 have been removed. This means that

  MOUNTING DEVICES THROUGH KDE WILL NO LONGER WORK WITHOUT HAL.

  Thus if you wish to use device icons or the media:/ view in Konqueror,
  make sure to turn on the HAL option in the x11/kdebase3 port and also
  read /usr/ports/UPDATING entry 20061219 and
  http://www.FreeBSD.org/gnome/docs/faq2.html#q19 for more information on
  using HAL.
</pre><p>For more information about KDE 3.5.6, see the KDE 3.5.6 <a href="http://www.kde.org/info/3.5.6.php">info page</a>.</p>