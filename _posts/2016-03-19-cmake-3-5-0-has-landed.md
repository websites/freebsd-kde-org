---
title: 'CMake 3.5.0 has landed'
date: 2016-03-19 00:00:00 
layout: post
---

<p>
Official ports have been updated to <a href="https://cmake.org/cmake/help/v3.5/release/3.5.html">CMake 3.5.0</a>. 
This brings CMake completely up-to-date with the upstream
release.
    </p>