---
title: 'KDE Frameworks 5.82 landed'
date: 2021-05-11 17:30
layout: post
---

The latest monthly version of KDE Frameworks is available
in the official FreeBSD ports tree. All the previous versions
landed nicely on-time as well, but we didn't write news posts for each.
