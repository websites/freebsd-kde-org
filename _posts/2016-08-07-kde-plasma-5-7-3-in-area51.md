---
title: 'KDE Plasma 5.7.3 in area51'
date: 2016-08-07 00:00:00 
layout: post
---

<p>
The unofficial ports tree for KDE-FreeBSD, <a href="https://freebsd.kde.org/area51.php">area51</a>,
has been updated with the newest releases of
<a href="https://www.kde.org/announcements/plasma-5.7.3.php">KDE Plasma</a>, version 5.7.3.
Users of the plasma5 branch of the area51 repository can update normally.
    </p>