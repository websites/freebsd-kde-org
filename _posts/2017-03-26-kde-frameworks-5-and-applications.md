---
title: 'KDE Frameworks 5 and Applications'
date: 2017-03-26 00:00:00 
layout: post
---

<p>KDE Frameworks 5 in ports have been update to the 
    <a href="https://www.kde.org/announcements/kde-frameworks-5.32.0.php">latest
    release</a> from KDE.org, version 5.32. The ports infrastructure has been
    updated to be ready for the current release of KDE Applications,
    version 16.12.3. The latter are not yet available in official ports,
    but they are in the <a href="https://freebsd.kde.org/area51.php">unofficial ports tree</a> in area51, in the plasma5/ branch.</p><p>This update ensures that when the KDE Applications for KF5 land,
    the infrastructure is ready for them.</p>