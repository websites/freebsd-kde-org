---
title: 'KDE Plasma 5.20.2 landed'
date: 2020-10-27 17:45:00
layout: post
---

The latest version of KDE Plasma Desktop is available in the official FreeBSD ports tree.
