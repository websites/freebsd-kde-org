---
title: 'KDE 4.2.4 in ports'
date: 2009-06-03 00:00:00 
layout: post
---

<p>KDE 4.2.4 has been committed to ports.</p><p>We're happy to announce that KDE 4.2.4 is now available in the FreeBSD ports tree. KDE 4.2.4 is only a bugfix release. A full changelog is available <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php">here</a>.</p><p>Happy updating!</p>