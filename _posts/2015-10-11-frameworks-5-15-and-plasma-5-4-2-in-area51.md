---
title: 'Frameworks 5.15 and Plasma 5.4.2 in area51'
date: 2015-10-11 00:00:00 
layout: post
---

<p>The KDE Frameworks 5 and Plasma 5 Desktop ports have been updated to the newly-released <a href="https://www.kde.org/announcements/kde-frameworks-5.15.0.php">KDE Frameworks 5.15.0</a> and <a href="https://www.kde.org/announcements/plasma-5.4.2.php">Plasma 5.4.2</a>, following the official releases at the beginning of October. These ports are intended for testing before eventually entering the official ports tree. These new ports are available in <a href="area51.php">area51</a> in the <i>plasma5</i> branch.
    </p>