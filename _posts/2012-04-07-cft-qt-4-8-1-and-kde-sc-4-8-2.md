---
title: 'CFT: Qt 4.8.1 and KDE SC 4.8.2'
date: 2012-04-07 00:00:00 
layout: post
---

<p>While we wait for the ports feature freeze to be over, we invite you to test the upcoming updates in the KDE land:
			<ul>
				<li>Qt 4.8.1;</li>
				<li>PyQt4 4.9.1;</li>
				<li><b>KDE SC 4.8.2</b>.</li>
			</ul>
		</p><pre>
$ svn co http://area51.pcbsd.org/trunk/area51
# sh Tools/scripts/kdemerge -kmpq /usr/ports
</pre><p>Since Qt started using the <i>raster graphics system engine</i> by default (and the native one seems not to be an option anymore), you should add...</p><pre>
kern.ipc.shmmni=1024
kern.ipc.shmseg=1024
</pre><p>...to <tt>/boot/loader.conf</tt>.</p>