---
title: 'Calligra 2.5 RC available for testing'
date: 2012-07-19 00:00:00 
layout: post
---

<p>While waiting for the final release, the KDE/FreeBSD team invites you to test the release candidate of the <b>Calligra Suite version 2.5</b> via the <a href="http://FreeBSD.kde.org/area51.php">area51</a> repository:</p><pre>
$ svn co http://area51.pcbsd.org/trunk/area51
# sh Tools/scripts/kdemerge -o /usr/ports
# portmaster editors/calligra
</pre><p>The official announcement can be found here: <a href="http://www.calligra.org/news/calligra-2-5-release-candidate">http://www.calligra.org/news/calligra-2-5-release-candidate</a>.</p>