---
title: 'KDE Frameworks 5.34 landed'
date: 2017-05-20 00:00:00 
layout: post
---

<p>The official ports for KDE Frameworks 5 have been updated
    to the <a href="https://www.kde.org/announcements/kde-frameworks-5.34.0.php">latest release</a>. Users of KDE Frameworks ports can update as usual.
    </p>