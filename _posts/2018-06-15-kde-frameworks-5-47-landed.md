---
title: 'KDE Frameworks 5.47 landed'
date: 2018-06-15 00:00:00 
layout: post
---

<p>
      Less than a week after their official release, the
      latest monthly version of KDE Frameworks is available
      in the official FreeBSD ports tree.
    </p>