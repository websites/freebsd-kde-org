---
title: 'Calligra 2.5.1 released'
date: 2012-09-01 00:00:00 
layout: post
---

<p>Shortly after 2.5 announcement, here comes the first bugfix release. You'll find an overview of the most important fixes in the <a href="http://www.calligra.org/news/calligra-2-5-1-released">official announcement</a>.</p>