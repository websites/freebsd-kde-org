---
title: 'KDE June Apps Update 20.04.2 landed'
date: 2020-06-13 10:15:00
layout: post
---

The latest monthly version of KDE Applications is available
in the official FreeBSD ports tree.
