---
title: 'Updates in area51'
date: 2016-04-20 00:00:00 
layout: post
---

<p>
The unofficial ports repository <a href="https://freebsd.kde.org/area51.php">area51</a> has been updated with a number of KDE Frameworks 5 and related ports: 
<a href="https://www.kde.org/announcements/kde-frameworks-5.21.0.php">KDE Frameworks 5</a> has been updated to version 5.21.0, released April 9th. <a href="https://www.kde.org/announcements/plasma-5.6.3.php">KDE Plasma 5</a> has been updated to 5.6.3, released April 19th. KDE Applications have been updated to the newly-released 16.04.
    </p>