---
title: 'KDE SC 4.9.5 comes to ports'
date: 2013-02-03 00:00:00 
layout: post
---

<p>The KDE/FreeBSD team presents KDE SC <b>4.9</b>.5, featuring, among new features and a lot of bug fixes, the new game <i>Pairs</i>, and split ports in the <i>kdemultimedia</i> and <i>kdenetwork</i> modules, allowing you to choose what you really want to install.</p>