---
title: 'KDE SC 4.6 roadmap'
date: 2011-03-01 00:00:00 
layout: post
---

<p>As you may know, we&#8217;ve had 4.6.0 in testing for about a month while the ports tree was in feature freeze. Now that it is open again, we are already dealing with 4.6.1 update, then <b>we&#8217;re not going to commit 4.6.0 to ports</b> just to replace it with <b>4.6.1 in few days</b>. In addition to not having to build KDE SC twice (not all that fun), you won&#8217;t experience some youth problems which should have been fixed in the latest release.</p><p>As a side note, we&#8217;re starting work on Qt 4.7.2, which will soon come to ports.</p><p>Enjoy the wait! :)</p>