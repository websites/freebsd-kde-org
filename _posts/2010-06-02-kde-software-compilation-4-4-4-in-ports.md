---
title: 'KDE Software Compilation 4.4.4 in ports'
date: 2010-06-02 00:00:00 
layout: post
---

<p>KDE SC ports have been updated to 4.4.4. See <a href="http://www.kde.org/announcements/announce-4.4.4.php">official announcement</a> for details.</p><p>New ports:</p><pre>
astro/marble (split from misc/kdeedu4):
    Virtual globe and world atlas for KDE.

graphics/kphotoalbum-kde4:
    Image viewer and organizer for KDE.
</pre>