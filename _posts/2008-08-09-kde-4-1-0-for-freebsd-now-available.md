---
title: 'KDE 4.1.0 for FreeBSD now available'
date: 2008-08-09 00:00:00 
layout: post
---

<p>On behalf of the FreeBSD KDE team, it is my great pleasure to announce that KDE 4 has been merged into the FreeBSD ports tree. The official KDE 4.1.0 release notes can be found at <a href="http://www.kde.org/announcements/4.1">http://www.kde.org/announcements/4.1</a> and FreeBSD users should also refer to the <a href="http://FreeBSD.kde.org/details/kde41.php">KDE 4.1 on FreeBSD notes</a>.</p>