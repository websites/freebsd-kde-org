---
title: 'digiKam 7.1.0 landed'
date: 2020-09-05 10:00
layout: post
---

The latest version of digiKam has landed in the FreeBSD ports tree.
