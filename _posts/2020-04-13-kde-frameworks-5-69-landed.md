---
title: 'KDE Frameworks 5.69 landed'
date: 2020-04-13 12:30:00
layout: post
---

The latest monthly version of KDE Frameworks is available
in the official FreeBSD ports tree.
