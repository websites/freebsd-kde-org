---
title: 'KDE Plasma 5.19.0 landed'
date: 2020-06-09 15:55:00
layout: post
---

The latest version of KDE Plasma Desktop is available in the official FreeBSD ports tree.
