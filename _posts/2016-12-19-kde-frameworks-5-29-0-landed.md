---
title: 'KDE Frameworks 5.29.0 landed'
date: 2016-12-19 00:00:00 
layout: post
---

<p>
The KDE-FreeBSD team is proud to introduce the
KDE Frameworks ports into the official FreeBSD ports tree.
The KDE Frameworks are 70 libraries that build on top of
Qt5 and provide lightweight, modular support for all kinds
of functionality for Qt5 applications. The KDE Frameworks
also serve as the platform on which KDE Plasma 5 and KDE Applications 
are built.
    </p><p>
This release includes all of the KDE Frameworks except
for kf5-wayland (because it requires Wayland, which isn't
in the ports tree yet) and kf5-bluez (because it doesn't make
sence on FreeBSD's Bluetooth stack).
    </p>