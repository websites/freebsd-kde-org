---
title: 'Frameworks 5.18 in area51'
date: 2016-01-15 00:00:00 
layout: post
---

<p>
      The monthly release of KDE Frameworks has also arrived in <a href="area51.php">area51</a>.
      Together with the newest KDE Frameworks ports,
      <a href="https://www.kde.org/announcements/plasma-5.5.3.php">Plasma</a> has been updated to 5.5.3
      and <a href="https://www.kde.org/announcements/announce-applications-15.12.1.php">KDE Applications</a> to 15.12.1.
      All these ports can be found in the unofficial ports tree.
    </p>