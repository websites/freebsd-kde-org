---
title: 'KDE/FreeBSD repository moved'
date: 2009-03-21 00:00:00 
layout: post
---

<p>The area51 repository has moved. This has been done to increase performance and reliability. It can now be accessed at <a href="http://area51.pcbsd.org">http://area51.pcbsd.org</a>. For more information, see <a href="http://FreeBSD.kde.org/area51.php">area51 instructions</a>.</p><p>Our thanks go to Kris and Josh at <a href="http://www.pcbsd.org">PC-BSD</a> for providing the hosting for this repository.</p><p><b>Please note:</b> the old repository has been powered down with immediate effect, due to severe hardware problems. It will not be coming back online at all.</p>