---
title: 'Kdevelop 4.3.0 available for testing'
date: 2012-04-04 00:00:00 
layout: post
---

<p>Thanks to the contribution of <i>Luca Pizzamiglio</i>, <b>Kdevelop</b> was updated to <b>4.3.0</b> in <a href="http://FreeBSD.kde.org/area51.php">area51</a>, and you're free to test it:</p><pre>
$ svn co http://area51.pcbsd.org/trunk/area51
# sh Tools/scripts/kdemerge -m /usr/ports
# portmaster devel/kdevelop-kde4
</pre><p>If no problems are found, it will probably be committed when KDE SC 4.8.2 comes to the tree.</p>