---
title: 'KDevelop reaches 4.4.1'
date: 2013-02-05 00:00:00 
layout: post
---

<p>The KDevelop 4.4 release comes packed with new features, bug fixes and improved performance. Also, new in this release is a shiny welcome screen for improved usability and an easier entry into the KDevelop world. As usual, the <a href="http://www.kdevelop.org/44/kdevelop-440-welcomes-you">official announcement</a> is the best source for information.</p>