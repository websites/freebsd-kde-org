---
title: 'Welcome KDE SC 4.14.3'
date: 2015-03-12 00:00:00 
layout: post
---

<p>The KDE Community proudly announces the latest -- and also the last planned -- release of Plasma Workspaces 4.11.14 and <a href="https://www.kde.org/announcements/announce-4.14.3.php">KDE Applications and Platform 4.14.3</a>. This version of Plasma Workspaces includes a fix for <a href="https://www.kde.org/info/security/advisory-20141106-1.txt">CVE-2014-8651</a>.</p>