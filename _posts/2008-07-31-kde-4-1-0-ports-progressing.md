---
title: 'KDE 4.1.0 ports progressing'
date: 2008-07-31 00:00:00 
layout: post
---

<p>KDE 4.1.0 porting is progressing.</p><p>At the moment, the ports are ready for testing before they are committed to the FreeBSD ports tree. If you want to help test them to speed up the process, visit <a href="http://wiki.FreeBSD.org/KDE4/Install">the FreeBSD wiki</a> and provide feedback.</p><p>See a <a href="http://FreeBSD.kde.org/screenshots.php">screenshot</a> of the test packages in action.</p>