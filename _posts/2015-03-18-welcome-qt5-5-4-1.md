---
title: 'Welcome Qt5 5.4.1'
date: 2015-03-18 00:00:00 
layout: post
---

<p>Qt5 has been in the ports tree for a little over a year, and today the KDE/FreeBSD team is happy to present the latest version, Qt 5.4.1, alongside Qt 4.8.6.</p>