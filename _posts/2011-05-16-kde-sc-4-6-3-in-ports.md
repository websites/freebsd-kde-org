---
title: 'KDE SC 4.6.3 in ports'
date: 2011-05-16 00:00:00 
layout: post
---

<p>We're pleased to announce the update to KDE Software Compilation 4.6.3. Read the full announcement <a href="http://kde.org/announcements/announce-4.6.3.php">here</a>.</p><p>Special thanks to Raphael Kubo da Costa for his work on this update.</p>