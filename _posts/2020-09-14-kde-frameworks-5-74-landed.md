---
title: 'KDE Frameworks 5.74 landed'
date: 2020-09-14 17:45
layout: post
---

The latest monthly version of KDE Frameworks is available
in the official FreeBSD ports tree.
