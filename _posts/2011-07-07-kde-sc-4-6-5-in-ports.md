---
title: 'KDE SC 4.6.5 in ports'
date: 2011-07-07 00:00:00 
layout: post
---

<p>The KDE/FreeBSD team is pleased to announce KDE Software Compilation 4.6.5. Read the <a href="http://kde.org/announcements/announce-4.6.5.php">original announcement</a> and the <a href="http://www.kde.org/announcements/changelogs/changelog4_6_4to4_6_5.php">changelog</a>.</p>