---
title: 'Calligra 2.4.2 now in ports'
date: 2012-05-31 00:00:00 
layout: post
---

<p>After a short incubation period, <b>Calligra Suite</b> has landed in the ports collection with his <b>brand new <a href="http://www.calligra.org/news/calligra-2-4-2-released">2.4.2 release</a></b>. Users of KOffice should move on to this new suite, deinstalling any KOffice port and installing equivalent Calligra ones (as noted in <tt>UPDATING</tt>).</p><p>To answer most people's question: future work on Calligra will indeed include splitting it into several ports featuring single applications.</p>