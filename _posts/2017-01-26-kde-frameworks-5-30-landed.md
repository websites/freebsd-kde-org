---
title: 'KDE Frameworks 5.30 landed'
date: 2017-01-26 00:00:00 
layout: post
---

<p>
    The official FreeBSD ports tree has been updated with this month's
    KDE Frameworks release, <a href="https://www.kde.org/announcements/kde-frameworks-5.30.0.php">version 5.30</a>. New in this release on
    the FreeBSD side is the inclusion of <i>x11/kf5-kwayland</i>, which
    opens the door to Wayland-compatible applications using the
    KDE Frameworks.
    </p>