---
title: 'Plasma 5.6 in area51'
date: 2016-03-23 00:00:00 
layout: post
---

<p>
Just a day after the release of <a href="https://dot.kde.org/2016/03/22/kde-plasma-56-release">Plasma 5.6</a>,
it has been added to the unofficial ports tree in the
<a href="area51.php">area51</a> repository.
The plasma5-branch in the repository builds the most modern and up-to-date Plasma shell.
The KDE Frameworks 5 and Plasma 5 are now co-installable with KDE4 from official ports,
now that the installation of headers has been untangled (since March 16th).
    </p>