---
title: 'KDE Plasma 5.18.1 landed'
date: 2020-02-19 18:30:00
layout: post
---

The latest version of KDE Plasma Desktop is available in the official FreeBSD ports tree.
