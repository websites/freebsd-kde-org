---
title: 'KDE Frameworks 5.78 landed'
date: 2021-01-18 17:30
layout: post
---

The latest monthly version of KDE Frameworks is available
in the official FreeBSD ports tree.
