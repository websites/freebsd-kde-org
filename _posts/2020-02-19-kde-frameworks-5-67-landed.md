---
title: 'KDE Frameworks 5.67 landed'
date: 2020-02-19 18:50:00
layout: post
---

The latest monthly version of KDE Frameworks is available
in the official FreeBSD ports tree.
