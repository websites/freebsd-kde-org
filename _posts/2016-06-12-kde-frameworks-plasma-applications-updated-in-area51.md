---
title: 'KDE Frameworks, Plasma, Applications updated in area51'
date: 2016-06-12 00:00:00 
layout: post
---

<p>
The unofficial ports for KDE-FreeBSD, <a href="https://freebsd.kde.org/area51.php">area51</a>,
have been updated with the newest releases from the KDE community:
<a href="https://www.kde.org/announcements/kde-frameworks-5.24.0.php">KDE Frameworks 5.24.0</a>,
<a href="https://www.kde.org/announcements/plasma-5.7.1.php">KDE Plasma 5.7.1</a>,
and <a href="https://www.kde.org/announcements/announce-applications-16.04.3.php">KDE Applications 16.04.3</a>.
All of these ports live in the plasma5 branch of the area51 repository.
    </p>