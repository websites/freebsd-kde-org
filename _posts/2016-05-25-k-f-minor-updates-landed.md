---
title: 'K-F Minor Updates landed'
date: 2016-05-25 00:00:00 
layout: post
---

<p>
Recently a whole bunch of small fixes were made in the ports
maintained by the KDE-FreeBSD team. The kdehier and kdeprefix ports
have finally been retired. New Qt5 ports have been added for the
Qt examples, and Qt Designer has been updated.
    </p>