---
title: 'Calligra, Krita updated to 3.0, KF5'
date: 2017-06-18 00:00:00 
layout: post
---

<p>
      The first applications built on KDE Frameworks 5 are
      beginning to arrive on FreeBSD. The Calligra suite
      of office and productivity applications and Krita,
      the painting application, have been updated to their
      respective 3.0 releases.
    </p><p>
      Users may want to uninstall calligra and kdiagram
      before building the newer version.
    </p>