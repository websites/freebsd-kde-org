---
title: 'KDE Applications updated'
date: 2018-08-25 00:00:00 
layout: post
---

<p>
      The august update to KDE Applications, including the packages
      of extragear maintained by the KDE-FreeBSD team, has landed
      in the official ports tree. Users can update normally.
    </p>