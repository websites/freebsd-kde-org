---
title: 'CMake 3.5.2 has landed'
date: 2016-04-18 00:00:00 
layout: post
---

<p>
The latest version of <a href="https://blog.kitware.com/cmake-3-5-2-available-for-download/">CMake</a>, 3.5.2, has been added to the official ports tree.
    </p>