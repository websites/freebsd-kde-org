---
title: 'Qt 4.8.4 and QtCreator 2.6.1 now available'
date: 2013-02-03 00:00:00 
layout: post
---

<p>The KDE/FreeBSD team presents Qt 4.8.4, and <b>QtCreator 2.6.1</b>, which now ships with a <i>QML designer</i>. Updating is strongly encouraged.</p>