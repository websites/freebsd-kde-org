---
title: 'Calligra 2.5.2 bugfix release available'
date: 2012-09-16 00:00:00 
layout: post
---

<p>Yet another Calligra release hit the ports tree. This new version <b>fixes some serious bugs</b> introduced in the first two 2.5.* releases. More details can be found in the <a href="http://www.calligra.org/news/calligra-2-5-2-released">official announcement</a>.</p>