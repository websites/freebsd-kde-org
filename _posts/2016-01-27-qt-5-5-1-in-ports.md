---
title: 'Qt 5.5.1 in ports'
date: 2016-01-27 00:00:00 
layout: post
---

<p>
In the official ports tree, Qt has been updated to Qt 5.5.1.
This is the culmination of months of porting effort, careful testing,
exp-runs and package-disentangling. Users of Qt5 on FreeBSD now
have the latest official Qt5 release available to them.
Users are advised to consult the UPDATING file, as some ports
have been split up.
Special thanks to Yuri Victorovich, who did an independent port of Qt 5.5.1
and whose work has been incorporated in this update.
    </p><p>Users of Qt4 are unaffected by this change.</p>