---
title: 'KDE Frameworks 5.48 landed'
date: 2018-07-19 00:00:00 
layout: post
---

<p>
      The latest monthly version of KDE Frameworks is available
      in the official FreeBSD ports tree.
    </p>