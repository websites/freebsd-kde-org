---
title: 'KDE 3.5.10 now available'
date: 2008-08-29 00:00:00 
layout: post
---

<p>Hot on the heels of KDE 3.5.9, the KDE/FreeBSD team is proud to announce the release of KDE 3.5.10 for FreeBSD. The official KDE 3.5.10 release notes can be found at:<br />
			<a href="http://www.kde.org/announcements/announce-3.5.10.php">http://www.kde.org/announcements/announce-3.5.10.php</a>.</p><p>While not a very exciting release in terms of features, 3.5.10 brings a couple of nice bugfixes and translation updates to those who choose to stay with KDE 3.5. The fixes are thinly spread across KPDF with a number of crash fixes, KGPG and probably most interesting various fixes in kicker, KDE's panel:<br />
			<ul>
				<li>improved visibility on transparent backgrounds;</li>
				<li>themed arrow buttons in applets that were missing them;</li>
				<li>layout and antialiasing fixes in various applets.</li>
			</ul>
		</p>