---
title: 'KDE 4.3.1 is in ports'
date: 2009-09-01 00:00:00 
layout: post
---

<p>KDE 4.3.1 has been committed to ports.</p><p>The KDE/FreeBSD team is proud to announce the release of KDE 4.3.1 for FreeBSD. The official KDE 4.3.1 notes can be found <a href="http://kde.org/announcements/announce-4.3.1.php">here</a>.</p><p>We'd like to say thanks to all helpers, testers and submitters.</p><p>Happy updating!</p>