---
title: 'Oxygen icons converging'
date: 2017-04-03 00:00:00 
layout: post
---

<p>The Oxygen icons which have colored the KDE4 desktop on FreeBSD
    for a long time are updated regularly upstream. However, they
    are no longer the Oxygen icons for KDE4, but for KDE Applications
    with KDE Frameworks 5. This means that icon updates do not happen
    for KDE4 from ports.</p><p>The KDE-FreeBSD team has renamed the port of the Oxygen icons
    from kde4-icons-oxygen to kf5-oxygen-icons5, and updated the 
    port with the latest upstream release. This is another small step
    in preparing for KDE Applications. While the port is now named
    "kf5-", it is not, in fact, based on Qt5 or KDE Frameworks 5,
    and can be used in a KDE4 environment with no change. The KDE4
    ports have been adjusted to use this newer version, even though
    the mix of KDE4 and KDE Frameworks 5 looks a little strange.
    </p>