---
title: 'Welcome KDE SC 4.10.1!'
date: 2013-05-27 00:00:00 
layout: post
---

<p>The KDE Community proudly announces the latest releases of Plasma Workspaces, Applications and Development Platform. Improved usability and performance, along with many visual refinements, help the Software Compilation make a further leap in quality. Read more on the <a href="http://kde.org/announcements/4.10">official announcement</a>.</p>