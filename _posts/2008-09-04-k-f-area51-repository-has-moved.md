---
title: "K-F 'area51' repository has moved"
date: 2008-09-04 00:00:00 
layout: post
---

<p>The KDE/FreeBSD repository 'area51' has now moved to a new server. Please see <a href="https://kf.athame.co.uk/access.php">https://kf.athame.co.uk/access.php</a> for more details.</p><p>The webpages here referring to the old location will be updated soon.</p>
