---
title: 'KDE Frameworks 5.35 in Area51'
date: 2017-06-11 00:00:00 
layout: post
---

<p>
      The latest release of KDE Frameworks 5.35 has been
      added to the <code>plasma5/</code> branch of the 
      <a href="https://github.com/freebsd/freebsd-ports-kde">Area51 repository</a>.
      Users of the experimental ports repository can upate and
      rebuild KDE Frameworks as usual.
    </p>