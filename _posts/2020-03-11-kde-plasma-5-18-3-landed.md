---
title: 'KDE Plasma 5.18.3 landed'
date: 2020-03-11 06:00:00
layout: post
---

The latest version of KDE Plasma Desktop is available in the official FreeBSD ports tree.
