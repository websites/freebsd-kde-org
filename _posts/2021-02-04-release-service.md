---
title: 'KDE Release Service 20.12.2 landed'
date: 2021-02-04 14:15:00
layout: post
---

The latest monthly version of applications, libraries and
other material from the KDE Release Service -- previously KDE Applications --
is available
in the official FreeBSD ports tree.
