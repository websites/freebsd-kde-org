---
title: 'KDE Frameworks 5.72 landed'
date: 2020-07-21 19:00
layout: post
---

The latest monthly version of KDE Frameworks is available
in the official FreeBSD ports tree.
