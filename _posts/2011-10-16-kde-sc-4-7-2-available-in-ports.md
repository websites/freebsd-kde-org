---
title: 'KDE SC 4.7.2 available in ports'
date: 2011-10-16 00:00:00 
layout: post
---

<p>The KDE/FreeBSD team is pleased to announce the release of KDE Software Compilation 4.7.2 for FreeBSD. We invite you to read the <a href="http://kde.org/announcements/announce-4.7.2.php">original announcement</a>.</p><p>A lot of effort was put into this and previous 4.7 releases both by the KDE and FreeBSD teams. Here's a list of some of the improvements:
			<ul>
				<li>some modules were split in smaller chunks, letting you install what you really want;</li>
				<li>startup time was reduced of approximately 15 seconds;</li>
				<li>KWin is now much lighter (on some systems almost doubled performance was registered);</li>
				<li>auto-login via KDM is now supported;</li>
				<li>Phonon-GStreamer will stop notifying you with annoying messages;</li>
				<li>Kopete finally supports GoogleTalk;</li>
				<li>Kalzium now installs with its molecular editor;</li>
				<li>Zeitgeist support was added.</li>
			</ul>
		</p><p>Along with this update, also Qt and PyQt4 were updated, respectively to 4.7.4 and 4.8.5.</p><p>More than ever, we'd like to say thanks to all testers and contributors, who were very helpful during this long porting process.</p>