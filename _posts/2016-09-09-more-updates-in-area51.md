---
title: 'More updates in area51'
date: 2016-09-09 00:00:00 
layout: post
---

<p>
The unofficial ports tree for KDE-FreeBSD, <a href="https://freebsd.kde.org/area51.php">area51</a>,
continues to keep pace with upstream KDE releases.
This week, it has been updated with 
<a href="https://www.kde.org/announcements/plasma-5.7.4.php">Plasma 5.7.4</a>
and
<a href="https://www.kde.org/announcements/announce-applications-16.08.1.php">KDE Applications 16.08.1</a>.
Users of the plasma5 branch of the area51 repository can update normally.
    </p>