---
title: 'Calligra 2.6.2: new features, and the new Calligra Author'
date: 2013-05-29 00:00:00 
layout: post
---

<p>Today we committed an important update to the Calligra Suite, result of thousand of commits which provide new features, polishing of the user interaction and tons of bug fixes.<br />
			Also, please welcome <b>Calligra Author</b> as the new member of the suite: it will support a writer in the process of <i>creating an eBook</i> from concept to publication.</p><p>For more information, read the <a href="http://www.calligra.org/news/calligra-2-6-released">official announcement</a>.</p>