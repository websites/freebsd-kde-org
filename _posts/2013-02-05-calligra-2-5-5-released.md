---
title: 'Calligra 2.5.5 released'
date: 2013-02-05 00:00:00 
layout: post
---

<p>The last bugfix release in the 2.5 branch of Calligra was committed to the ports; it brings several improvements and thus we recommend everybody to update.</p>