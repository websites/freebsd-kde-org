---
title: 'Qt 5.11.2 landed'
date: 2018-09-25 00:00:00 
layout: post
---

<p>
The Qt ports have been updated to version 5.11.2. This continues
to be a "frankenQt" with an older QtWebEngine port.
    </p>