---
title: 'KDE Plasma 5.18.0 landed'
date: 2020-05-06 16:30:00
layout: post
---

The latest version of KDE Plasma Desktop is available in the official FreeBSD ports tree.
