---
title: 'digiKam 4.14 in area51'
date: 2016-03-29 00:00:00 
layout: post
---

<p>
The latest version of <a href="https://www.digikam.org/">digiKam</a>
for KDE4 technology, digiKam 4.14, has been added to the
unofficial ports tree in area51. The trunk of that repository
reflects what is "up next" for merging into the official ports.
    </p>