---
title: 'KDE Plasma 5.18.0 landed'
date: 2020-02-11 18:00:00
layout: post
---

The latest version of KDE Plasma Desktop is available in the official FreeBSD ports tree.
