---
title: 'KDE Frameworks 5.79 landed'
date: 2021-02-15 17:30
layout: post
---

The latest monthly version of KDE Frameworks is available
in the official FreeBSD ports tree.
