---
title: 'KDE Plasma 5.18.4.1 landed'
date: 2020-04-01 05:00:00
layout: post
---

The latest version of KDE Plasma Desktop is available in the official FreeBSD ports tree.
