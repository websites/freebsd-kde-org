---
title: 'DigiKam 4.14.0 has landed'
date: 2016-04-14 00:00:00 
layout: post
---

<p>
<a href="https://www.digikam.org/">DigiKam</a> and related ports have been updated in the official ports tree to 4.14.0.
</p><p>
This is a huge leap, as we are moving from 4.2.0 (released in August 2014) to
4.14.0 (released in October 2015). Version 4.14.0 is the last KDE4-based
release, and 5.0.0 will be based on KDE Frameworks 5.
    </p>