---
title: 'KDE4 reorganization has landed'
date: 2016-03-16 00:00:00 
layout: post
---

<p>
Official ports have been updated with a new structure for KDE4 installation.
The header installation location for kdelibs4-based ports has changed to
include/kde4 instead of include (which
consequently causes several other ports to have their installation paths
changed too).</p><p>The idea behind this is to reduce path conflicts between KDE4 ports and the
upcoming KDE Frameworks 5 ports that will be installed into include/KF5.
    </p>