---
title: 'Frameworks 5.16 and Plasma 5.4.3 in area51'
date: 2015-11-25 00:00:00 
layout: post
---

<p>The KDE Frameworks 5 and Plasma 5 Desktop ports have been updated to the newly-released <a href="https://www.kde.org/announcements/kde-frameworks-5.16.0.php">KDE Frameworks 5.16.0</a> and <a href="https://www.kde.org/announcements/plasma-5.4.3.php">Plasma 5.4.3</a>. There are also ports of the <a href="https://www.kde.org/announcements/announce-applications-15.12-beta.php">Applications 15.12 beta</a>. These ports are intended for testing before eventually entering the official ports tree. These new ports are available in <a href="area51.php">area51</a> in the <i>plasma5</i> branch.
    </p>