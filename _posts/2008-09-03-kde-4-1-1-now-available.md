---
title: 'KDE 4.1.1 now available'
date: 2008-09-03 00:00:00 
layout: post
---

<p>The KDE/FreeBSD team is proud to announce the release of KDE 4.1.1 for FreeBSD. The official KDE 4.1.1 release notes can be found at:<br />
			<a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>.</p><p>KDE community ships first translation and service release of the 4.1 free desktop, containing numerous bugfixes, performance improvements and translation updates.</p><p>Pretty much all applications have received the developers' attention, resulting in a long list of bugfixes and improvements. The most significant changes are:<br />
			<ul>
				<li>significant performance, interaction and rendering correctness improvements in KHTML and Konqueror, KDE's web browser;</li>
				<li>user interaction, rendering and stability fixes in Plasma, the KDE desktop shell;</li>
				<li>PDF backend fixes in the document viewer Okular;</li>
				<li>fixes in Gwenview, the image viewer's thumbnailing, more robust retrieval and display of images with broken metadata;</li>
				<li>stability and interaction fixes in KMail.</li>
			</ul>
		</p><p>New ports:</p><pre>
graphics/kcoloredit:
    KColorEdit is a palette files editor. It can be used for editing color
    palettes and for color choosing and naming.

graphics/kgraphviewer:
    KGraphViewer is a GraphViz DOT graph viewer for KDE. The GraphViz programs
    are free software layout engines for graphs. KGraphViewer displays the
    graphs in a modern, user-friendly GUI with all the power of a well
    integrated KDE application.

graphics/kiconedit:
    KIconEdit is designed to help create icons for KDE using the standard icon
    palette.

graphics/skanlite:
    Skanlite is a simple image scanning application that does nothing more than
    scan and save images. Skanlite can open a save dialog for every image
    scanned or save the images immediately in a specified directory with
    auto-generated names and format. The user can also choose to show the
    scanned image before saving.
</pre>