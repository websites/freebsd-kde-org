---
title: 'KDE Frameworks 5.73 landed'
date: 2020-08-09 15:30
layout: post
---

The latest monthly version of KDE Frameworks is available
in the official FreeBSD ports tree.
