---
title: 'Welcome KDE SC 4.12.5'
date: 2014-05-10 00:00:00 
layout: post
---

<p>The KDE Community proudly announces the latest releases of Plasma Workspaces 4.11.9 and <a href="http://www.kde.org/announcements/announce-4.12.5.php">KDE Applications 4.12.5</a>.</p>