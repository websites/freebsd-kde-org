---
title: 'KOffice 1.6.3 in ports'
date: 2007-07-04 00:00:00 
layout: post
---

<p>KOffice 1.6.3 has been committed to ports.</p><p>For more information, read the <a href="http://www.koffice.org/announcements/announce-1.6.3.php">official announcement</a> and the <a href="http://www.koffice.org/announcements/changelog-1.6.3.php">changelog</a>.</p>