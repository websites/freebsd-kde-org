---
title: 'KDE4 Updates'
date: 2017-03-29 00:00:00 
layout: post
---

<p>Although KDE4 software -- based on kdelibs4 -- is no longer 
    actively developed upstream, KDE4 is the only complete version
    of KDE software currently available from ports. It receives 
    occasional updates upstream, and therefore occasional updates
    in FreeBSD.</p><p>KDE4 has been updated to the latest workspace release,
    4.11.22, and latest kdelibs release, 4.14.30.
    </p>