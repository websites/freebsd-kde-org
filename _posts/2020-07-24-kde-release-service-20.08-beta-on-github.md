---
title: 'KDE Release Service 20.08-beta available in the development tree'
date: 2020-07-24 18:30:00
layout: post
---

The beta version of KDE Release Service is available in the [release-service-20.08.0 branch](https://github.com/freebsd/freebsd-ports-kde/tree/release-service-20.08.0) on our github ports tree.
