---
title: 'Applications, Calligra, Frameworks, Plasma updated in area51'
date: 2015-12-21 00:00:00 
layout: post
---

<p>
      Many of the KDE-related ports in <a href="area51.php">area51</a> have been updated once again. 
      The Calligra suite has been updated to <a href="https://www.calligra.org/news/calligra-2-9-10-released/">version 2.9.10</a>.
      <a href="https://www.kde.org/announcements/announce-applications-15.12.0.php">KDE Applications</a> are version 15.12.0.
      The Plasma desktop has updated to (the weekly release) <a href="https://www.kde.org/announcements/plasma-5.5.1.php">Plasma 5.5.1</a>.
      All these ports can be found in the unofficial ports tree.
    </p>