---
title: 'New Frameworks 5.14.0 in area51'
date: 2015-09-12 00:00:00 
layout: post
---

<p>The "bleeding edge" repository for KDE Frameworks 5 ports has been updated to the newly-released <a href="https://www.kde.org/announcements/kde-frameworks-5.14.0.php">KDE Frameworks 5.14.0</a> and <a href="https://www.kde.org/announcements/plasma-5.4.1.php">Plasma 5.4.1</a>. These new ports are intended for testing before eventually entering the official ports tree, and have been built by the KDE-FreeBSD team on FreeBSD 9.3 x86, 10.2 amd64 and 11 amd64. These new ports are available in <a href="area51.php">area51</a> in the <i>plasma5</i> branch.
    </p>