---
title: 'Preparations for Frameworks and Plasma5'
date: 2015-07-10 00:00:00 
layout: post
---

<p>Work continues apace on the "bleeding edge" ports for the next generation of <a href="https://www.kde.org/announcements/kde-frameworks-5.12.0.php">KDE Frameworks</a>, <a href="https://www.kde.org/announcements/announce-applications-15.04.3.php">Applications</a> and the <a href="https://www.kde.org/announcements/plasma-5.3.2.php">Plasma5 workspace</a>. This is available through <a href="area51.php">area51</a>, the bleeding-edge development area for KDE/FreeBSD ports.</p>