---
title: 'Qt 4.8.1 and KDE SC 4.8.3 in ports, at last!'
date: 2012-05-25 00:00:00 
layout: post
---

<p>The long awaited <b>KDE SC 4.8.3</b> was just committed to ports, along with <b>Qt 4.8.1</b>, Phonon 4.6.0, PyQt 4.9.1, and many more updates you shouldn't care about. What you should be aware of is that <b>KDE-Pim</b> was finally updated and put in sync with the Software Compilation. As <tt>UPDATING</tt> suggests, if you want to stick with 4.4.11.1, you can replace <tt>deskutils/kdepim4</tt> with <tt>deskutils/kdepim44</tt>.</p>