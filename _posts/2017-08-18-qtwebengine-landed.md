---
title: 'QtWebEngine landed'
date: 2017-08-18 00:00:00 
layout: post
---

<p>
      The long-awaited QtWebEngine -- the Qt port of the Blink engine that
      also powers the Chromium browser -- has landed in the official ports
      tree. This completes the portfolio of Qt5 modules on FreeBSD,
      and paves the way for browser- and application updates that
      depend on a reasonably-modern web-rendering engine.
    </p>