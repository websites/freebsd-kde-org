---
title: 'Frameworks 5.19 and other updates in area51'
date: 2016-02-19 00:00:00 
layout: post
---

<p>
      The monthly release of KDE Frameworks has also arrived in <a href="area51.php">area51</a>.
      Together with the newest <a href="https://www.kde.org/announcements/kde-fra meworks-5.17.0.php">KDE Frameworks</a> ports,
      <a href="https://www.kde.org/announcements/announce-applications-15.12.2.php">KDE Applications</a> to 15.12.2
      and <a href="http://milianw.de/blog/kdevelop-50-beta-2-and-473-releases">KDevelop 5.0</a> has been updated to beta2.
      All these ports can be found in the unofficial ports tree in the plasma5 branch.
    </p>