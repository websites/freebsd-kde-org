---
title: 'Welcome KDE SC 4.12.4'
date: 2014-03-03 00:00:00 
layout: post
---

<p>The KDE Community proudly announces the latest releases of Plasma Workspaces 4.11.8 and <a href="http://www.kde.org/announcements/announce-4.12.4.php">KDE Applications 4.12.4</a>.</p>