---
title: 'Plasma 5.6 beta in area51'
date: 2016-03-05 00:00:00 
layout: post
---

<p>
The <a href="https://dot.kde.org/2016/03/02/plasma-56-beta">Plasma 5.6</a> beta release 
has been added to the <a href="area51.php">area51</a> repository. This means that the plasma5-branch in area51 now builds and installs a beta.
Users can Update their plasma5-plasma port from the unnoficial
ports tree. Use r12023 for the Plasma 5.5.5 release.
    </p>