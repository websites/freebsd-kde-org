---
title: 'KDE-FreeBSD in 2019'
date: 2019-12-30 00:00:00 
layout: post
---

The KDE-FreeBSD website did not update much in 2019.

It updated in one important way, though: it switched from
a PHP-and-Capacity backend to a Jekyll-and-Markdown backend,
which makes it match the rest of the KDE community websites
(those that are not wiki's). Special thanks to Carl Schwan
for making that happen.

The important-but-changeable information remains on
the [community wiki](https://community.kde.org/FreeBSD).

## CMake updates in 2019

CMake was updates to 3.13, 3.14, 3.15 and 3.16 lands in
early january 2020. There were several patch-release updates
to CMake.

Each CMake update brings with it an exp-run and thousands of
packages being re-built. The KDE-FreeBSD team tries to fix
fallout directly before committing the new CMake version.

## Qt updates in 2019

Qt updated to 5.12.0, 5.12.1, 5.12.2, 5.13.0 and 5.13.2 in
the course of the year. We received important contributions
from Kai Knobich to bring Qt WebEngine up-to-date again,
and from Piotr Kubaj for PowerPC compatibility.

## KDE updates in 2019

The [KDE Frameworks](https://kde.org/products/frameworks/)
are a collection of 80-or-so lightweight modular libraries
built on top of Qt, licensed under the LGPL. They follow
a monthly release cadence, so there were plenty of updates
during the year. KDE Frameworks are CI-tested by the
KDE community on FreeBSD, so those updates are generally
low-impact.

Plasma Desktop releases bug-fixes every month, and
new releases every quarter. All of these releases
were pushed to ports in short order.

KDE Applications -- now the KDE release service, which
provides libraries and applications following a regular
schedule -- updates regularly as well. There were a few
surprises in the course of the year, but most of
the updates were low-impact.

Outside of the KDE release service, Krita and Digikam and
kdenlive and KDevelop were updated with each release and are now up-to-date
with upstream.

