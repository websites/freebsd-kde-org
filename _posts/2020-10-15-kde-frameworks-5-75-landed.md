---
title: 'KDE Frameworks 5.75 landed'
date: 2020-10-15 17:30
layout: post
---

The latest monthly version of KDE Frameworks is available
in the official FreeBSD ports tree.
