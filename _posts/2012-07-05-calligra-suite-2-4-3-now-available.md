---
title: 'Calligra Suite 2.4.3 now available'
date: 2012-07-05 00:00:00 
layout: post
---

<p>KDE office suite, <b>Calligra</b>, has been updated to its latest version, <b>2.4.3</b>. You're all invited to upgrade, and to have a look at the <a href="http://www.calligra.org/news/calligra-2-4-3-released">release notes</a>. Meanwhile, release 2.5.0 is approaching, with cool new features!</p>