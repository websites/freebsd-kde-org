---
title: 'KDE Frameworks 5.66 landed'
date: 2020-01-15 16:00:00
layout: post
---

The latest monthly version of KDE Frameworks is available
in the official FreeBSD ports tree.
