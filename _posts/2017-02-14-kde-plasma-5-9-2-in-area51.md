---
title: 'KDE Plasma 5.9.2 in area51'
date: 2017-02-14 00:00:00 
layout: post
---

<p>
    The KDE-FreeBSD team loves announcing that area51 is all up-to-date
    with the latest software releases from the KDE community.
    The <a href="https://freebsd.kde.org/area51.php">unofficial ports tree</a>
    for KDE-FreeBSD now contains <a href="https://www.kde.org/announcements/kde-frameworks-5.31.0.php">KDE Frameworks 5.31</a>,
<a href="https://www.kde.org/announcements/plasma-5.9.2.php">KDE Plasma Desktop 5.9.2</a> and
<a href="https://www.kde.org/announcements/announce-applications-16.12.2.php">KDE Applications 16.12</a>.
    </p>