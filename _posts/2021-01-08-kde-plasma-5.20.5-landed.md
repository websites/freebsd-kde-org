---
title: 'KDE Plasma 5.20.5 landed'
date: 2021-01-08 17:45:00
layout: post
---

The latest bugfix release of KDE Plasma Desktop is available in the official FreeBSD ports tree.
